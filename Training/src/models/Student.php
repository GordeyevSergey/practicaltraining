<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Student".
 *
 * @property int $id
 * @property string $name
 * @property string $groupname
 */
class Student extends \yii\db\ActiveRecord
{

public $subjectbuf;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Student';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'groupname'], 'required'],
            [['name', 'groupname'], 'string', 'max' => 30],
            [['subjectbuf'],'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'groupname' => 'Groupname',
        ];
    }

    public function getGroup()
                        {
                            return $this->hasOne(Groups::className(),['groupname' => 'groupname']);
                        }

    public function getLinks()
                        {
                            return $this->hasMany(StudentSubject::className(),['studentid' => 'id']);
                        }


}
