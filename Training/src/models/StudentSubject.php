<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Student_subject".
 *
 * @property int $id
 * @property int $studentid
 * @property int $subjectid
 */
class StudentSubject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Student_subject';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['studentid', 'subjectid'], 'required'],
            [['studentid', 'subjectid'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'studentid' => 'Studentid',
            'subjectid' => 'Subjectid',
        ];
    }

     public function getStudent()
                            {
                                return $this->hasOne(Student::className(),['studentid' => 'id']);
                            }

     public function getSubject()
                            {
                                return $this->hasOne(Subject::className(),['subjectnid' => 'id']);
                            }

}
