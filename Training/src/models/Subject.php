<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Subject".
 *
 * @property int $id
 * @property string $subjectname
 */
class Subject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Subject';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subjectname'], 'required'],
            [['subjectname'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subjectname' => 'Subjectname',
        ];
    }
}
