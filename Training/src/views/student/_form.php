<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Groups;
use app\models\Student;
use app\models\Subject;

/* @var $this yii\web\View */
/* @var $model app\models\Student */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'groupname')->dropDownList(ArrayHelper::map(Groups::find()->all(),'groupname','groupname')) ?>

    <?= $form->field($model, 'subjectbuf')->checkBoxList(ArrayHelper::map(Subject::find()->all(),'id','subjectname')) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
